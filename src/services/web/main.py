from fastapi import FastAPI

from src.services.web.routes.main_ruter import main_router
from src.services.web.config import app_settings as settings

# from src.services.database.app.init_db import create_first_superuser

app = FastAPI(
    title=settings.app.APP_TITLE, description=settings.app.DESCRIPTION
)

app.include_router(main_router)


# @app.on_event('startup')
# async def startup():
#     await create_first_superuser()
