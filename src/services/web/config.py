from pydantic_settings import BaseSettings, SettingsConfigDict

from src.services.settings_schemas import (
    ConstantsConfig,
    AppConfig,
)


class AppSettings(BaseSettings):

    app: AppConfig
    constants: ConstantsConfig

    model_config = SettingsConfigDict(
        env_file=".env", env_nested_delimiter="__", extra="ignore"
    )


app_settings = AppSettings()
