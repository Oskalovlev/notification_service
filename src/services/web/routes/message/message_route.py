import uuid

from fastapi import APIRouter, Depends
from sqlalchemy.ext.asyncio import AsyncSession

from src.services.database.app.database import get_async_session

from src.services.database.repository import message_repository
from src.services.database.app.user_config import current_superuser
from src.services.web.ulils.dependencies import messages_by_id
from src.domain.entities import (
    MessageModel as Message,
    MessageCreateSchema,
    MessageUpdateSchema,
    MessageDBSchema,
)

router = APIRouter()


@router.post(
    "/",
    response_model=MessageDBSchema,
)
async def create_new_message(
    message: MessageCreateSchema,
    session: AsyncSession = Depends(get_async_session),
) -> Message:

    new_message = await message_repository.create(message, session)
    return new_message


@router.get(
    "/",
    response_model=list[MessageDBSchema],
)
async def get_all_message(
    session: AsyncSession = Depends(get_async_session),
) -> list[Message]:

    all_messages = await message_repository.get_multi(session)
    return all_messages


@router.get("/statistic/", response_model=list[MessageDBSchema])
async def get_messages_order_by_status(
    session: AsyncSession = Depends(get_async_session),
) -> list[Message]:
    return await message_repository.get_messages_by_status(session=session)


@router.get("/statistic/{status}", response_model=list[MessageDBSchema])
async def get_messages_grouped_by_status(
    status: str, session: AsyncSession = Depends(get_async_session)
) -> list[Message]:
    return await message_repository.get_messages_status(
        status, session=session
    )


@router.patch(
    "/{message_id}",
    response_model=MessageDBSchema,
)
async def update_message(
    message: MessageUpdateSchema,
    message_id: Message = Depends(messages_by_id),
    session: AsyncSession = Depends(get_async_session),
) -> Message:

    update_message = await message_repository.update(
        message_id, message, session
    )
    return update_message


@router.delete(
    "/{message_id}",
    response_model=MessageDBSchema,
    dependencies=[Depends(current_superuser)],
)
async def delete_message(
    message_id: uuid.UUID,
    session: AsyncSession = Depends(get_async_session),
) -> None:

    message = await message_repository.remove(message_id, session)
    return message
