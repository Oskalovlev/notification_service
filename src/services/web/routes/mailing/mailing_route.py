import uuid

from fastapi import APIRouter, Depends
from sqlalchemy.ext.asyncio import AsyncSession

from src.services.database.app.database import get_async_session
from src.services.database.repository import mailing_repository
from src.services.database.app.user_config import current_superuser
from src.services.web.ulils.dependencies import mailing_by_id
from src.domain.entities import (
    MailingModel,
    MailingCreateSchema,
    MailingUpdateSchema,
    MailingDBSchema,
)

router = APIRouter()


@router.post(
    "/",
    response_model=MailingDBSchema,
)
async def create_new_mailing(
    mailing: MailingCreateSchema,
    session: AsyncSession = Depends(get_async_session),
) -> MailingModel:

    return await mailing_repository.create_mailing(mailing, session)


@router.get(
    "/",
    response_model=list[MailingDBSchema],
)
async def get_all_mailing(
    session: AsyncSession = Depends(get_async_session),
) -> list[MailingModel]:

    return await mailing_repository.get_multi(session)


@router.patch("/{mailing_id}/", response_model=MailingDBSchema)
async def update_mailing(
    mailing_update: MailingUpdateSchema,
    mailing: MailingModel = Depends(mailing_by_id),
    session: AsyncSession = Depends(get_async_session),
) -> MailingModel:

    return await mailing_repository.update(mailing, mailing_update, session)


@router.delete(
    "/{mailing_id}",
    response_model=MailingDBSchema,
    dependencies=[Depends(current_superuser)],
)
async def delete_mailing(
    mailing_id: uuid.UUID,
    session: AsyncSession = Depends(get_async_session),
) -> None:

    mailing = await mailing_repository.remove(mailing_id, session)
    return mailing
