from fastapi import APIRouter

from src.services.web.routes import (
    user_router,
    client_router,
    mailing_router,
    message_router,
)

main_router = APIRouter()

main_router.include_router(user_router)
main_router.include_router(client_router, prefix="/client", tags=["Client"])
main_router.include_router(mailing_router, prefix="/mailing", tags=["Mailing"])
main_router.include_router(message_router, prefix="/message", tags=["Message"])
