from src.services.web.routes.user.user_route import (  # noqa
    router as user_router,
)
from src.services.web.routes.client.client_route import (  # noqa
    router as client_router,
)
from src.services.web.routes.mailing.mailing_route import (  # noqa
    router as mailing_router,
)
from src.services.web.routes.message.message_route import (  # noqa
    router as message_router,
)
