import uuid

from fastapi import APIRouter, Depends
from sqlalchemy.ext.asyncio import AsyncSession

from src.services.database.app.database import get_async_session
from src.services.database.repository import client_repository
from src.services.database.app.user_config import current_superuser
from src.services.web.ulils.dependencies import client_by_id
from src.domain.entities import (
    ClientModel as Client,
    ClientCreateSchema,
    # ClientUpdateSchema,
    ClientUpdatePartialSchema,
    ClientDBSchema,
)

router = APIRouter()


@router.post(
    "/",
    response_model=ClientDBSchema,
)
async def create_new_client(
    client: ClientCreateSchema,
    session: AsyncSession = Depends(get_async_session),
) -> Client:

    return await client_repository.create_client(client, session)


@router.get(
    "/", response_model=list[ClientDBSchema], response_model_exclude_none=True
)
async def get_all_clients(
    session: AsyncSession = Depends(get_async_session),
) -> list[Client]:

    all_clients = await client_repository.get_clients(session)
    return all_clients


@router.get(
    "/{client_id}/",
    response_model=ClientDBSchema,
    response_model_exclude_none=True,
)
async def get_clients_by_id(
    client_id: uuid.UUID,
    session: AsyncSession = Depends(get_async_session),
) -> list[Client]:

    return await client_repository.get(client_id, session)


@router.patch(
    "/{client_id}/",
    response_model=ClientDBSchema,
)
async def update_client_partial(
    client_update: ClientUpdatePartialSchema,
    client: Client = Depends(client_by_id),
    session: AsyncSession = Depends(get_async_session),
) -> Client:

    return await client_repository.update(client, client_update, session)


@router.delete(
    "/{client_id}/",
    response_model=ClientDBSchema,
    dependencies=[Depends(current_superuser)],
)
async def delete_client(
    client_id,
    session: AsyncSession = Depends(get_async_session),
) -> None:

    return await client_repository.remove(client_id, session)
