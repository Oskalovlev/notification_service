import uuid
from typing import Annotated

from fastapi import Path, Depends, HTTPException, status
from sqlalchemy.ext.asyncio import AsyncSession

from src.services.database.app.database import get_async_session
from src.services.database.repository import (
    client_repository,
    message_repository,
    mailing_repository,
)
from src.domain.entities import (
    ClientModel as Client,
    MailingModel as Mailing,
    MessageModel as Message,
)


async def client_by_id(
    client_id: Annotated[uuid.UUID, Path],
    session: AsyncSession = Depends(get_async_session),
) -> Client:
    client = await client_repository.get(client_id, session)
    if client is not None:
        return client

    raise HTTPException(
        status_code=status.HTTP_404_NOT_FOUND,
        detail=f"Client {client_id} not found!",
    )


async def mailing_by_id(
    mailing_id: Annotated[uuid.UUID, Path],
    session: AsyncSession = Depends(get_async_session),
) -> Mailing:
    mailing = await mailing_repository.get(mailing_id, session)
    if mailing is not None:
        return mailing

    raise HTTPException(
        status_code=status.HTTP_404_NOT_FOUND,
        detail=f"Mailing {mailing_id} not found!",
    )


async def messages_by_id(
    message_id: Annotated[uuid.UUID, Path],
    session: AsyncSession = Depends(get_async_session),
) -> Message:

    message = await message_repository.get(message_id, session)
    if message is not None:
        return message

    raise HTTPException(
        status_code=status.HTTP_404_NOT_FOUND,
        detail=f"Message {message_id} not found!",
    )
