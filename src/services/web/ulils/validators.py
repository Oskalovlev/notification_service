import uuid
from datetime import datetime

from fastapi import HTTPException, status
from sqlalchemy.ext.asyncio import AsyncSession

from src.services.database.repository import mailing_repository
from src.domain.entities import MailingModel


async def check_mailing_exists(
    mailing_id: uuid.UUID,
    session: AsyncSession,
):
    mailing = await mailing_repository.get(mailing_id, session)
    if mailing is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Рассылка не найдена!",
        )
    return mailing


async def check_correct_mailing_datetime(mailing: MailingModel):
    return mailing.datetime_at <= datetime.now() < mailing.datetime_to


async def get_all_client_for_datetime(client):
    if check_correct_mailing_datetime:
        return client.all()


async def check_duplicate_mailing(
    mailing_text_body: str, session: AsyncSession
):
    text_body = await mailing_repository.get_mailing_text_body(
        mailing_text_body, session
    )

    if text_body is not None:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Такая рассылка уже существует!",
        )
