from typing import Optional

from pydantic import BaseModel, EmailStr


class AppConfig(BaseModel):
    APP_TITLE: str = "Notification"
    DESCRIPTION: str = "Service"


class DatabaseConfig(BaseModel):

    DB_HOST: str
    DB_PORT: str
    DB_NAME: str
    DB_USER: str
    DB_PASS: str

    @property
    def DATABASE_URL_asyncpg(self):
        return (
            f"postgresql+asyncpg://{self.DB_USER}:{self.DB_PASS}@"
            f"{self.DB_HOST}:{self.DB_PORT}/{self.DB_NAME}"
        )


class ConstantsConfig(BaseModel):

    SECRET: str = "SECRET"

    FIRST_SUPERUSER_EMAIL: Optional[EmailStr] = None
    FIRST_SUPERUSER_PASSWORD: Optional[str] = None

    ZERO: int = 0
    LENGTH_NAME: int = 100
    MIN_ANYSTR_LENGTH: int = 1
    MIN_LENGTH_PASS: int = 3
    LIFETIME_JWT: int = 3600
