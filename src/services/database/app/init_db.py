import contextlib

from fastapi_users.exceptions import UserAlreadyExists
from pydantic import EmailStr

from src.services.database.app.config import db_settings as settings
from src.services.database.app.database import get_async_session
from src.services.database.app.user_config import get_user_db, get_user_manager
from src.domain.entities import UserCreate

get_async_session_context = contextlib.asynccontextmanager(get_async_session)
get_user_db_context = contextlib.asynccontextmanager(get_user_db)
get_user_manager_context = contextlib.asynccontextmanager(get_user_manager)


async def create_user(
    email: EmailStr, password: str, is_superuser: bool = False
):
    try:
        async with get_async_session_context() as session:
            async with get_user_db_context(session) as user_db:
                async with get_user_manager_context(user_db) as user_manager:
                    await user_manager.create(
                        UserCreate(
                            email=email,
                            password=password,
                            is_superuser=is_superuser,
                        )
                    )
    except UserAlreadyExists:
        pass


async def create_first_superuser():
    if (
        settings.constants.FIRST_SUPERUSER_EMAIL is not None
        and settings.constants.FIRST_SUPERUSER_PASSWORD is not None
    ):
        await create_user(
            email=settings.constants.FIRST_SUPERUSER_EMAIL,
            password=settings.constants.FIRST_SUPERUSER_PASSWORD,
            is_superuser=True,
        )
