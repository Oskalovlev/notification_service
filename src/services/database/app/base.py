"""Импорты класса Base и всех моделей для Alembic."""

from src.domain.entities import MailingModel, ClientModel  # noqa
from src.services.database.app.database import Base  # noqa
