from sqlalchemy import select
from sqlalchemy.engine import Result
from sqlalchemy.ext.asyncio import AsyncSession

from src.services.database.repository.base_repository import BaseRepository
from src.domain.entities import ClientModel as Client, ClientCreateSchema


class ClientRepository(BaseRepository):
    @staticmethod
    async def create_client(
        client_in: ClientCreateSchema, session: AsyncSession
    ) -> Client:
        client = Client(**client_in.model_dump())
        session.add(client)
        await session.commit()
        await session.refresh(client)
        return client

    @staticmethod
    async def get_clients(session: AsyncSession) -> list[Client]:
        stmt = select(Client).order_by(Client.id)
        result: Result = await session.execute(stmt)
        clients = result.scalars().all()
        return list(clients)


client_repository = ClientRepository(Client)
