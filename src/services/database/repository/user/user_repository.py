from src.services.database.repository.base_repository import BaseRepository
from src.domain.entities import UserModel


class UserRepository(BaseRepository):
    pass


user_repository = UserRepository(UserModel)
