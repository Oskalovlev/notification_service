import uuid
from typing import Optional

from fastapi.encoders import jsonable_encoder
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from src.domain.entities import UserModel as User


class BaseRepository:
    def __init__(self, model):
        self.model = model

    # async def get(
    #     self,
    #     obj_id,
    #     session: AsyncSession,
    # ):
    #     db_obj = await session.execute(
    #         select(self.model).where(self.model.id == obj_id)
    #     )
    #     return db_obj.scalars().first()

    #     # return await session.get(self.model, obj_id)

    async def get(self, id: uuid.UUID, session: AsyncSession | None = None):

        query = select(self.model).where(self.model.id == id)
        response = await session.execute(query)
        return response.scalar_one_or_none()

    async def get_multi(self, session: AsyncSession):
        db_objs = await session.execute(select(self.model))
        return db_objs.scalars().all()

    async def create(
        self, obj_in, session: AsyncSession, user: Optional[User] = None
    ):
        obj_in_data = obj_in.model_dump()
        if user is not None:
            obj_in_data["user_id"] = user.id
        db_obj = self.model(**obj_in_data)
        session.add(db_obj)
        await session.commit()
        await session.refresh(db_obj)
        return db_obj

    async def update(
        self,
        db_obj,
        obj_in,
        session: AsyncSession,
    ):
        obj_data = jsonable_encoder(db_obj)
        update_data = obj_in.dict(exclude_unset=True)

        for field in obj_data:
            if field in update_data:
                setattr(db_obj, field, update_data[field])
        session.add(db_obj)
        await session.commit()
        await session.refresh(db_obj)
        return db_obj

    async def remove(
        self,
        id: uuid.UUID,
        session: AsyncSession,
    ):
        response = await session.execute(
            select(self.model).where(self.model.id == id)
        )
        obj = response.scalar_one()
        await session.delete(obj)
        await session.commit()
        return obj
