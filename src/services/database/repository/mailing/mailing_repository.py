from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from src.services.database.repository.base_repository import BaseRepository
from src.domain.entities import MailingModel as Mailing, MailingCreateSchema


class MailingRepository(BaseRepository):
    @staticmethod
    async def create_mailing(
        mailing_in: MailingCreateSchema, session: AsyncSession
    ) -> Mailing:
        mailing = Mailing(**mailing_in.model_dump())
        session.add(mailing)
        await session.commit()
        await session.refresh(mailing)
        return mailing

    @staticmethod
    async def get_mailing_text_body(
        mailing_text_body,
        session: AsyncSession,
    ):
        mailing_text_body = await session.execute(
            select(Mailing.id).where(Mailing.text_body == mailing_text_body)
        )
        return mailing_text_body.scalars().first()


mailing_repository = MailingRepository(Mailing)
