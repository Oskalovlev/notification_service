from src.services.database.repository.user.user_repository import (  # noqa
    user_repository,
)
from src.services.database.repository.client.client_repository import (  # noqa
    client_repository,
)
from src.services.database.repository.mailing.mailing_repository import (  # noqa
    mailing_repository,
)
from src.services.database.repository.message.message_repository import (  # noqa
    message_repository,
)
