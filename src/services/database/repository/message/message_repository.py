from sqlalchemy import select
from sqlalchemy.engine import Result
from sqlalchemy.ext.asyncio import AsyncSession

from src.services.database.repository.base_repository import BaseRepository
from src.domain.entities import MessageModel as Message


class MessageRepository(BaseRepository):
    @staticmethod
    async def get_messages_by_status(
        session: AsyncSession,
    ) -> list[Message]:

        stmt = select(Message).order_by(Message.send_status)
        result: Result = await session.execute(stmt)
        messages_order_by_status = result.scalars().all()
        return list(messages_order_by_status)

    @staticmethod
    async def get_messages_status(
        status,
        session: AsyncSession,
    ) -> list[Message]:

        stmt = select(Message).where(Message.send_status == status)
        result: Result = await session.execute(stmt)
        messages_filter_status = result.scalars().all()
        return list(messages_filter_status)


message_repository = MessageRepository(Message)
