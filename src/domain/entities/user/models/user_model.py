from fastapi_users_db_sqlalchemy import SQLAlchemyBaseUserTableUUID

# from src.services.database.app.database import Base
from src.domain.entities.base_model import BaseModel


class UserModel(SQLAlchemyBaseUserTableUUID, BaseModel):
    pass
