from datetime import datetime

from pydantic import ConfigDict

from src.domain.entities.base_schema import (
    PydanticBaseSchema,
    PydanticDBSchema,
)


class MailingBaseSchema(PydanticBaseSchema):

    datetime_at: datetime
    text_body: str
    property_filter: str
    datetime_to: datetime


class MailingCreateSchema(MailingBaseSchema):

    pass


class MailingUpdateSchema(MailingCreateSchema):

    datetime_at: datetime | None = None
    text_body: str | None = None
    property_filter: str | None = None
    datetime_to: datetime | None = None


class MailingDBSchema(MailingBaseSchema, PydanticDBSchema):

    model_config = ConfigDict(from_attributes=True)
