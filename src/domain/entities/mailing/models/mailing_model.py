from datetime import datetime

from sqlalchemy import func
from sqlalchemy.orm import Mapped, mapped_column

from src.domain.entities.base_model import BaseModel


class MailingModel(BaseModel):

    datetime_at: Mapped[datetime] = mapped_column(default=func.now())
    text_body: Mapped[str]
    property_filter: Mapped[str]
    datetime_to: Mapped[datetime] = mapped_column(default=func.now())
