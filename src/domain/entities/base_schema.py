import uuid

from pydantic import BaseModel


class PydanticDBSchema(BaseModel):

    __abstract__ = True

    id: uuid.UUID


class PydanticBaseSchema(BaseModel):

    __abstract__ = True
