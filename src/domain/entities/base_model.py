from sqlalchemy.orm import Mapped

from src.services.database.app.database import Base
from src.domain.entities.short_annotate import short_annotate


class BaseModel(Base):

    __abstract__ = True

    id: Mapped[short_annotate.uuidpk]
