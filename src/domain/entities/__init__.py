from src.domain.entities.mailing import (  # noqa
    MailingModel,
    MailingCreateSchema,
    MailingUpdateSchema,
    MailingDBSchema,
)

from src.domain.entities.client import (  # noqa
    ClientModel,
    ClientCreateSchema,
    ClientUpdateSchema,
    ClientUpdatePartialSchema,
    ClientDBSchema,
)

from src.domain.entities.message import (  # noqa
    MessageModel,
    MessageCreateSchema,
    MessageUpdateSchema,
    MessageDBSchema,
)

from src.domain.entities.user import (  # noqa
    UserModel,
    UserCreate,
    UserRead,
    UserUpdate,
)
