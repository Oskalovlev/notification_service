from pydantic import ConfigDict

from src.domain.entities.base_schema import (
    PydanticBaseSchema,
    PydanticDBSchema,
)


class ClientBaseSchema(PydanticBaseSchema):

    phone: str
    code: str
    tag: str
    zone: str


class ClientCreateSchema(ClientBaseSchema):

    pass


class ClientUpdateSchema(ClientCreateSchema):

    pass


class ClientUpdatePartialSchema(ClientCreateSchema):

    phone: str | None = None
    code: str | None = None
    tag: str | None = None
    zone: str | None = None


class ClientDBSchema(ClientBaseSchema, PydanticDBSchema):

    model_config = ConfigDict(from_attributes=True)
