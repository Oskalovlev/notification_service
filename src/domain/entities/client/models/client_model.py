from sqlalchemy.orm import Mapped

from src.domain.entities.base_model import BaseModel


class ClientModel(BaseModel):

    phone: Mapped[str]
    code: Mapped[str]
    tag: Mapped[str]
    zone: Mapped[str]
