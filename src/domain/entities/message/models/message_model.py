import uuid
from datetime import date

from sqlalchemy import text, UUID, ForeignKey
from sqlalchemy.orm import Mapped, mapped_column

from src.domain.entities.base_model import BaseModel


class MessageModel(BaseModel):

    created_at: Mapped[date] = mapped_column(
        server_default=text("TIMEZONE('utc', now())")
    )
    send_status: Mapped[str]
    id_client: Mapped[uuid.UUID] = mapped_column(
        UUID, ForeignKey("client.id", ondelete="CASCADE")
    )
    id_mailing: Mapped[uuid.UUID] = mapped_column(
        UUID, ForeignKey("mailing.id", ondelete="CASCADE")
    )
