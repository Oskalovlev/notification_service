import uuid
from datetime import date

from pydantic import ConfigDict

from src.domain.entities.base_schema import (
    PydanticBaseSchema,
    PydanticDBSchema,
)


class MessageBaseSchema(PydanticBaseSchema):

    created_at: date
    send_status: str
    id_client: uuid.UUID
    id_mailing: uuid.UUID


class MessageCreateSchema(MessageBaseSchema):

    pass


class MessageUpdateSchema(MessageCreateSchema):

    created_at: date | None = None
    send_status: str | None = None
    id_client: uuid.UUID | str = None
    id_mailing: uuid.UUID | str = None


class MessageDBSchema(MessageBaseSchema, PydanticDBSchema):

    model_config = ConfigDict(from_attributes=True)
    # id: uuid.UUID
