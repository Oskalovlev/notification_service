"""Init

Revision ID: cb6e7121cd74
Revises:
Create Date: 2024-02-28 22:26:38.378839

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa
import fastapi_users_db_sqlalchemy


# revision identifiers, used by Alembic.
revision: str = "cb6e7121cd74"
down_revision: Union[str, None] = None
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table(
        "client",
        sa.Column("phone", sa.String(), nullable=False),
        sa.Column("code", sa.String(), nullable=False),
        sa.Column("tag", sa.String(), nullable=False),
        sa.Column("timezone", sa.DateTime(), nullable=False),
        sa.Column("id", sa.UUID(), nullable=False),
        sa.PrimaryKeyConstraint("id"),
    )
    op.create_table(
        "mailing",
        sa.Column("datetime_at", sa.DateTime(), nullable=False),
        sa.Column("text_body", sa.String(), nullable=False),
        sa.Column("property_filter", sa.String(), nullable=False),
        sa.Column("datetime_to", sa.DateTime(), nullable=False),
        sa.Column("id", sa.UUID(), nullable=False),
        sa.PrimaryKeyConstraint("id"),
    )
    op.create_table(
        "user",
        sa.Column(
            "id", fastapi_users_db_sqlalchemy.generics.GUID(), nullable=False
        ),
        sa.Column("email", sa.String(length=320), nullable=False),
        sa.Column("hashed_password", sa.String(length=1024), nullable=False),
        sa.Column("is_active", sa.Boolean(), nullable=False),
        sa.Column("is_superuser", sa.Boolean(), nullable=False),
        sa.Column("is_verified", sa.Boolean(), nullable=False),
        sa.PrimaryKeyConstraint("id"),
    )
    with op.batch_alter_table("user", schema=None) as batch_op:
        batch_op.create_index(
            batch_op.f("ix_user_email"), ["email"], unique=True
        )

    op.create_table(
        "message",
        sa.Column(
            "created_at",
            sa.Date(),
            server_default=sa.text("TIMEZONE('utc', now())"),
            nullable=False,
        ),
        sa.Column("send_status", sa.Boolean(), nullable=False),
        sa.Column("id_client", sa.UUID(), nullable=False),
        sa.Column("id_mailing", sa.UUID(), nullable=False),
        sa.Column("id", sa.UUID(), nullable=False),
        sa.ForeignKeyConstraint(
            ["id_client"], ["client.id"], ondelete="CASCADE"
        ),
        sa.ForeignKeyConstraint(
            ["id_mailing"], ["mailing.id"], ondelete="CASCADE"
        ),
        sa.PrimaryKeyConstraint("id"),
    )
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table("message")
    with op.batch_alter_table("user", schema=None) as batch_op:
        batch_op.drop_index(batch_op.f("ix_user_email"))

    op.drop_table("user")
    op.drop_table("mailing")
    op.drop_table("client")
    # ### end Alembic commands ###
