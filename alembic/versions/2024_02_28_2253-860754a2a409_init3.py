"""Init3

Revision ID: 860754a2a409
Revises: 6c930235dcf3
Create Date: 2024-02-28 22:53:16.439072

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision: str = "860754a2a409"
down_revision: Union[str, None] = "6c930235dcf3"
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    with op.batch_alter_table("client", schema=None) as batch_op:
        batch_op.add_column(sa.Column("zone", sa.String(), nullable=False))
        batch_op.drop_column("time_zone")

    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    with op.batch_alter_table("client", schema=None) as batch_op:
        batch_op.add_column(
            sa.Column(
                "time_zone", sa.VARCHAR(), autoincrement=False, nullable=False
            )
        )
        batch_op.drop_column("zone")

    # ### end Alembic commands ###
